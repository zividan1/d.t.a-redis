# **Deploy Redis On a Kubernetes Cluster - Idan Ziv**
## Using Kubernetes resources / helm templates to achieve a working solution.
I've used kali linux to deploy, but this guide should be working the same for all debian based linux systems.

Starting this task all I had was a basic understanding of linux. Other than that I've never used any of these tools, so understanding what I'm doing and what are all of the right steps to get there took me about 3 work days in total. (This is also the first readme file I've ever written).

### Part 1 - Docker Installation
a. Perform APT updating
```bash
sudo apt update
```
b. Get PGP key for official Docker
```bash
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
```
c. Configure APT to download, install, and update Docker
```bash
echo 'deb [arch=amd64] https://download.docker.com/linux/debian buster stable' |
sudo tee /etc/apt/sources.list.d/docker.list
```
d. Update the APT again
```bash
sudo apt update
```
e. Remove old version if exists
```bash
sudo apt remove docker docker-engine docker.io
```
f. Install Docker
```bash
sudo apt install docker-ce -y
```
g. Set up Docker to start automatically on reboot
```bash
sudo systemctl enable docker
```
h. Start Docker
```bash
sudo systemctl start docker
```
i. Verify installation
```bash
sudo docker run hello-world
```

### Part 2 - Kubernetes Installation
a. Install packages needed to use the Kubernetes apt repository.
Make sure to perform APT updating if you skipped the **Docker Installation** part (`sudo apt update`)
```bash
sudo apt-get install -y apt-transport-https ca-certificates curl
```
b. Download the Google Cloud public signing key
```bash
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
```
c. Add the Kubernetes apt repository
```bash
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
```
d. Update apt package index with the new repository and install kubectl
```bash
sudo apt-get update
sudo apt-get install -y kubectl
```
e. Download and install Minikube
```bash
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube
```
f. Configure Docker as the driver for Minikube
```bash
minikube config set driver docker
```
g. Add your user to a docker group in order to get permissions
```bash
sudo usermod -aG docker $USER && newgrp docker
```
h. Start Minikube
```bash
minikube start
```

### Part 3 - Helm Installation
a. install packages needed to use the Helm apt repository
```bash
curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -
sudo apt-get install apt-transport-https --yes
echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
```
b. Update apt package index with the new repository and install Helm
```bash
sudo apt-get update
sudo apt-get install helm
```

### Part 4 - Install and Deploy Redis Using Bitnami's Helm Chart
a. Add Bitnami's repository to Helm
```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
```
b. Deploy the redis chart, while also configure the configmap files to change Redis logging verbosity level to debug mode.
Note that the cluster name will be calld `my-redis` and the redis password will be `tikal123`. you may change both of them to a name and password of your choice.
```bash
helm install my-redis \
  --set auth.password=tikal123 \
  --set master.configuration="loglevel debug" \
  --set replica.configuration="loglevel debug" \
    bitnami/redis
```
Note that with this Helm chart, security context is enabled by default.
#
This deployment is using the default Master-Replicas cluster topology and also many other default settings provided by Bitnami.

Reffer to the Helm chart's Github repository for more options and custom configurations:
https://github.com/bitnami/charts/blob/master/bitnami/redis/README.md

